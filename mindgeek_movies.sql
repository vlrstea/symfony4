-- --------------------------------------------------------
-- Server:                       127.0.0.1
-- Versiune server:              10.1.33-MariaDB - mariadb.org binary distribution
-- SO server:                    Win32
-- HeidiSQL Versiune:            9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Descarcă structura bazei de date pentru mindgeek_movies
CREATE DATABASE IF NOT EXISTS `mindgeek_movies` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `mindgeek_movies`;

-- Descarcă structura pentru tabelă mindgeek_movies.cert
CREATE TABLE IF NOT EXISTS `cert` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Descarcă datele pentru tabela mindgeek_movies.cert: ~0 rows (aproximativ)
/*!40000 ALTER TABLE `cert` DISABLE KEYS */;
/*!40000 ALTER TABLE `cert` ENABLE KEYS */;

-- Descarcă structura pentru tabelă mindgeek_movies.genres
CREATE TABLE IF NOT EXISTS `genres` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Descarcă datele pentru tabela mindgeek_movies.genres: ~0 rows (aproximativ)
/*!40000 ALTER TABLE `genres` DISABLE KEYS */;
/*!40000 ALTER TABLE `genres` ENABLE KEYS */;

-- Descarcă structura pentru tabelă mindgeek_movies.media
CREATE TABLE IF NOT EXISTS `media` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subjectid` int(11) NOT NULL,
  `mediapath` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Descarcă datele pentru tabela mindgeek_movies.media: ~0 rows (aproximativ)
/*!40000 ALTER TABLE `media` DISABLE KEYS */;
/*!40000 ALTER TABLE `media` ENABLE KEYS */;

-- Descarcă structura pentru tabelă mindgeek_movies.migration_versions
CREATE TABLE IF NOT EXISTS `migration_versions` (
  `version` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Descarcă datele pentru tabela mindgeek_movies.migration_versions: ~1 rows (aproximativ)
/*!40000 ALTER TABLE `migration_versions` DISABLE KEYS */;
INSERT INTO `migration_versions` (`version`) VALUES
	('20180709003154');
/*!40000 ALTER TABLE `migration_versions` ENABLE KEYS */;

-- Descarcă structura pentru tabelă mindgeek_movies.movies
CREATE TABLE IF NOT EXISTS `movies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `duration` int(11) DEFAULT NULL,
  `year` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Descarcă datele pentru tabela mindgeek_movies.movies: ~0 rows (aproximativ)
/*!40000 ALTER TABLE `movies` DISABLE KEYS */;
/*!40000 ALTER TABLE `movies` ENABLE KEYS */;

-- Descarcă structura pentru tabelă mindgeek_movies.people
CREATE TABLE IF NOT EXISTS `people` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Descarcă datele pentru tabela mindgeek_movies.people: ~0 rows (aproximativ)
/*!40000 ALTER TABLE `people` DISABLE KEYS */;
/*!40000 ALTER TABLE `people` ENABLE KEYS */;

-- Descarcă structura pentru tabelă mindgeek_movies.rel_table
CREATE TABLE IF NOT EXISTS `rel_table` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subjectid_id` int(11) NOT NULL,
  `typeid` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_2192B5CAB0AB6A97` (`subjectid_id`),
  CONSTRAINT `FK_2192B5CAB0AB6A97` FOREIGN KEY (`subjectid_id`) REFERENCES `movies` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Descarcă datele pentru tabela mindgeek_movies.rel_table: ~0 rows (aproximativ)
/*!40000 ALTER TABLE `rel_table` DISABLE KEYS */;
/*!40000 ALTER TABLE `rel_table` ENABLE KEYS */;

-- Descarcă structura pentru tabelă mindgeek_movies.reviews
CREATE TABLE IF NOT EXISTS `reviews` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subjectid` int(11) NOT NULL,
  `reviewid` bigint(20) DEFAULT NULL,
  `lastupdated` date DEFAULT NULL,
  `quote` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rating` int(11) DEFAULT NULL,
  `reviewauthor` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `skygoid` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `skygourl` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sum` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `synopsis` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `startdate` date DEFAULT NULL,
  `enddate` date DEFAULT NULL,
  `waytowatch` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Descarcă datele pentru tabela mindgeek_movies.reviews: ~0 rows (aproximativ)
/*!40000 ALTER TABLE `reviews` DISABLE KEYS */;
/*!40000 ALTER TABLE `reviews` ENABLE KEYS */;

-- Descarcă structura pentru tabelă mindgeek_movies.type
CREATE TABLE IF NOT EXISTS `type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Descarcă datele pentru tabela mindgeek_movies.type: ~0 rows (aproximativ)
/*!40000 ALTER TABLE `type` DISABLE KEYS */;
/*!40000 ALTER TABLE `type` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
