const fetchData = document.getElementById('fetchData');
fetchData.addEventListener('click', ajaxCall);


function ajaxCall(){

	const proxyurl = "https://cors-anywhere.herokuapp.com/";
	const url = "https://mgtechtest.blob.core.windows.net/files/showcase.json";

	const xhr = new XMLHttpRequest();

	xhr.open('GET', proxyurl + url, true);
	xhr.onload = function(){
		if(this.status = 200){
			sendJSONToController(this.responseText);
		}
	}

	xhr.onerror = function(){

		console.log('AJAX request error');
	}

	xhr.send();
}

function sendJSONToController(data){

	const xhr = new XMLHttpRequest();
	//const data = '{"name":"John", "age":31, "city":"New York"}';
	xhr.open('POST', '/getJSON', true);
	xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded;charset=UTF-8");
	xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest')
	xhr.send(data);
	xhr.onload = function(){

		if(this.status == 200){
			document.getElementById('test').innerHTML = this.responseText;
		}
	}

	xhr.onerror = function(){
		console.log('Couldn\'t send data to the controller')
	};


}