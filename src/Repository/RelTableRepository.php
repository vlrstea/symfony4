<?php

namespace App\Repository;

use App\Entity\RelTable;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method RelTable|null find($id, $lockMode = null, $lockVersion = null)
 * @method RelTable|null findOneBy(array $criteria, array $orderBy = null)
 * @method RelTable[]    findAll()
 * @method RelTable[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RelTableRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, RelTable::class);
    }

//    /**
//     * @return RelTable[] Returns an array of RelTable objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?RelTable
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
