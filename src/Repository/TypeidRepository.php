<?php

namespace App\Repository;

use App\Entity\Typeid;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Typeid|null find($id, $lockMode = null, $lockVersion = null)
 * @method Typeid|null findOneBy(array $criteria, array $orderBy = null)
 * @method Typeid[]    findAll()
 * @method Typeid[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TypeidRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Typeid::class);
    }

//    /**
//     * @return Typeid[] Returns an array of Typeid objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Typeid
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
