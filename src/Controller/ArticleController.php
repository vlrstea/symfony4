<?php
namespace App\Controller;
use App\Entity\Article;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route; 
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ArticleController extends Controller{

	/** 
		* @Route("/test")
		* @Method({"GET"})
	 */
	public function index(){

		//return new Response('<html><body>Hello</body></html>');
	
		return $this->render('articles/index.html.twig');
	}


	/**
	 * @Route("/article/save")
	 * 
	 */
	public function save(){
		$em = $this->getDoctrine()->getManager();

		$article = new Article();
		$article->setTitle('Article One');
		$article->setBody('This is the body for article one');

		$em->persist($article);
		$em->flush();

		return new Response('Saved an article with an id of' . $article->getId() . 'and title' . $article->getTitle());
	}

}