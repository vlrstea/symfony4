<?php
namespace App\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route; 
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

use App\Entity\Movies;
use App\Entity\Cert;

class HomeController extends Controller{


	//Just render the page
	/**
	 * @Route("/");
	 */

	public function index(){

		return $this->render('index.html.twig'); 
	}



	//Get the json with AJAX and send it to doctrine
	/**
	 * @Route("/getJSON")
	 */
	
	public function getJSON(){
		$request = Request::createFromGlobals();
		//json formated
		//ajax request
		//echo $request->request->get();
		$movies = json_decode($request->getContent(), true);

		// foreach($movies as $movie){

		// 	$this->sendTODB($movie);
		// }

		return new JsonResponse($json[0]);

	}

	protected function sendTODB(array $movie){

		//Entity Manager
		$em = $this->getDoctrine()->getManager();

		$movie = new Movies();
		$movie->setTitle($movie['headline']);
		$movie->setBody($movie['body']);
		$movie->setDuration($movie['duration']);
		$movie->setYear($movie['year']);
		$movie->setQuote($movie['quote']);
		$movie->setRating($movie['rating']);
		$movie->setSynopsis($movie['synopsis']);

		$cert = new Cert();
		$cert->setType();



	}
}