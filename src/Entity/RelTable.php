<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\RelTableRepository")
 */
class RelTable
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $typeid;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Movies", inversedBy="relations")
     * @ORM\JoinColumn(nullable=false)
     */
    private $subjectid;

    public function getId()
    {
        return $this->id;
    }

    public function getType(): ?string
    {
        return $this->typeid;
    }

    public function setType(string $typeid): self
    {
        $this->typeid = $typeid;

        return $this;
    }

    public function getSubjectid(): ?Movies
    {
        return $this->subjectid;
    }

    public function setSubjectid(?Movies $subjectid): self
    {
        $this->subjectid = $subjectid;

        return $this;
    }


}
