<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ReviewsRepository")
 */
class Reviews
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $subjectid;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $reviewid;

    
    private $reviewauthor;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $skygoid;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $skygourl;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $sum;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $url;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $startdate;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $enddate;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $waytowatch;

    public function getId()
    {
        return $this->id;
    }

    public function getSubjectid(): ?int
    {
        return $this->subjectid;
    }

    public function setSubjectid(int $subjectid): self
    {
        $this->subjectid = $subjectid;

        return $this;
    }

    public function getReviewid(): ?int
    {
        return $this->reviewid;
    }

    public function setReviewid(?int $reviewid): self
    {
        $this->reviewid = $reviewid;

        return $this;
    }

    public function getLastupdated(): ?\DateTimeInterface
    {
        return $this->lastupdated;
    }

    public function setLastupdated(?\DateTimeInterface $lastupdated): self
    {
        $this->lastupdated = $lastupdated;

        return $this;
    }

    public function getReviewauthor(): ?string
    {
        return $this->reviewauthor;
    }

    public function setReviewauthor(?string $reviewauthor): self
    {
        $this->reviewauthor = $reviewauthor;

        return $this;
    }

    public function getSkygoid(): ?string
    {
        return $this->skygoid;
    }

    public function setSkygoid(string $skygoid): self
    {
        $this->skygoid = $skygoid;

        return $this;
    }

    public function getSkygourl(): ?string
    {
        return $this->skygourl;
    }

    public function setSkygourl(?string $skygourl): self
    {
        $this->skygourl = $skygourl;

        return $this;
    }

    public function getSum(): ?string
    {
        return $this->sum;
    }

    public function setSum(?string $sum): self
    {
        $this->sum = $sum;

        return $this;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(?string $url): self
    {
        $this->url = $url;

        return $this;
    }

    public function getStartdate(): ?\DateTimeInterface
    {
        return $this->startdate;
    }

    public function setStartdate(?\DateTimeInterface $startdate): self
    {
        $this->startdate = $startdate;

        return $this;
    }

    public function getEnddate(): ?\DateTimeInterface
    {
        return $this->enddate;
    }

    public function setEnddate(?\DateTimeInterface $enddate): self
    {
        $this->enddate = $enddate;

        return $this;
    }

    public function getWaytowatch(): ?string
    {
        return $this->waytowatch;
    }

    public function setWaytowatch(?string $waytowatch): self
    {
        $this->waytowatch = $waytowatch;

        return $this;
    }
}
