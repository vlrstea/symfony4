<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MediaRepository")
 */
class Media
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $type;

    /**
     * @ORM\Column(type="integer")
     */
    private $subjectid;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $mediapath;

    public function getId()
    {
        return $this->id;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getSubjectid(): ?int
    {
        return $this->subjectid;
    }

    public function setSubjectid(int $subjectid): self
    {
        $this->subjectid = $subjectid;

        return $this;
    }

    public function getMediapath(): ?string
    {
        return $this->mediapath;
    }

    public function setMediapath(string $mediapath): self
    {
        $this->mediapath = $mediapath;

        return $this;
    }
}
