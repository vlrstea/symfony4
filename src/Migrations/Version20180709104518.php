<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180709104518 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE movies ADD quote VARCHAR(255) DEFAULT NULL, ADD rating INT DEFAULT NULL, ADD synopsis VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE reviews DROP lastupdated, DROP quote, DROP rating, DROP reviewauthor, DROP synopsis');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE movies DROP quote, DROP rating, DROP synopsis');
        $this->addSql('ALTER TABLE reviews ADD lastupdated DATE DEFAULT NULL, ADD quote VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci, ADD rating INT DEFAULT NULL, ADD reviewauthor VARCHAR(100) DEFAULT NULL COLLATE utf8mb4_unicode_ci, ADD synopsis VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci');
    }
}
