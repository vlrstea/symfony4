<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180709003154 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE cert (id INT AUTO_INCREMENT NOT NULL, type VARCHAR(50) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE genres (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(30) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE media (id INT AUTO_INCREMENT NOT NULL, type VARCHAR(50) NOT NULL, subjectid INT NOT NULL, mediapath VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE movies (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(255) NOT NULL, body LONGTEXT NOT NULL, duration INT DEFAULT NULL, year INT DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE people (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(100) NOT NULL, role VARCHAR(20) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE rel_table (id INT AUTO_INCREMENT NOT NULL, subjectid_id INT NOT NULL, typeid INT NOT NULL, INDEX IDX_2192B5CAB0AB6A97 (subjectid_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE reviews (id INT AUTO_INCREMENT NOT NULL, subjectid INT NOT NULL, reviewid BIGINT DEFAULT NULL, lastupdated DATE DEFAULT NULL, quote VARCHAR(255) DEFAULT NULL, rating INT DEFAULT NULL, reviewauthor VARCHAR(100) DEFAULT NULL, skygoid VARCHAR(100) NOT NULL, skygourl VARCHAR(255) DEFAULT NULL, sum VARCHAR(100) DEFAULT NULL, synopsis VARCHAR(255) DEFAULT NULL, url VARCHAR(255) DEFAULT NULL, startdate DATE DEFAULT NULL, enddate DATE DEFAULT NULL, waytowatch VARCHAR(100) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE type (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(50) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE rel_table ADD CONSTRAINT FK_2192B5CAB0AB6A97 FOREIGN KEY (subjectid_id) REFERENCES movies (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE rel_table DROP FOREIGN KEY FK_2192B5CAB0AB6A97');
        $this->addSql('DROP TABLE cert');
        $this->addSql('DROP TABLE genres');
        $this->addSql('DROP TABLE media');
        $this->addSql('DROP TABLE movies');
        $this->addSql('DROP TABLE people');
        $this->addSql('DROP TABLE rel_table');
        $this->addSql('DROP TABLE reviews');
        $this->addSql('DROP TABLE type');
    }
}
